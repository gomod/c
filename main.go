package c

import (
	"fmt"

	"gitlab.com/gomod/f"
)

func Say(l int) {
	fmt.Printf("%v%v\n", "                       "[:l*2], "C: 1.3.0")
	f.Say(l + 1)
}
